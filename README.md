---
nav: exclude
---

# eccentricOrange.github.io

[![Netlify Status](https://api.netlify.com/api/v1/badges/3b80a397-e5fa-41ff-8913-3b28d0dd628a/deploy-status)](https://app.netlify.com/sites/eccentricorange/deploys)

Personal blogs
